import styled from 'styled-components'

export const PokeCard = styled.div.attrs((props) => ({
    changeGradient: props.changeGradient || "black",
    changeGradient2: props.changeGradient2 || "black"
    }))`
    text-align: center;
    width: 16.6%;
    min-width: 175px;
    margin: 10px 10px;
    padding-bottom: 1.2%;
    padding-top: 1.2%;
    transition: 0.7s;
    cursor: pointer;
    box-shadow: 0 0 6px black;
    border-radius: 10px;
    &:hover{
        color: black;
        transform: scale(1.1);
        background: rgb(60,54,54);
        background: linear-gradient(216deg, ${ props => props.changeGradient } 20%, ${ props => props.changeGradient2 } 100%);
    }
`