export default function TagEstilo(cor) {
    const estilo = {
        backgroundColor: `${cor}`,
        padding: '0 3% 1.5% 3%',
        borderRadius: '10%',
        fontSize: '0.9em',
    }
    return estilo;
}