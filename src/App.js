import React from 'react'

import UserPage from './Pages/UserPage'
import MainPage from './Pages/MainPage'
import LoginUser from './Pages/LoginUser'
import NotFound from './Pages/NotFound'
import PokeInfo from './Pages/PokeInfo'
import './Styles/CorDeFundo.css'
import Footer from './Components/Footer'

import { Router } from '@reach/router'
import axios from 'axios'

axios.get("https://pokedex20201.herokuapp.com/pokemons")
.then((resp) => 'console.log(resp)')
.catch((err) => console.warn('Deu n guerreiro'))


const App = () => {
  return (
  <>
    <Router>
      <LoginUser path="/" />
      <MainPage path="/main/:num" />
      <UserPage path="/profile/:username"/>
      <PokeInfo path="/pokeinfo/:name"/>
      <NotFound default />
    </Router>
    <Footer />
  </>
  )
}

export default App;