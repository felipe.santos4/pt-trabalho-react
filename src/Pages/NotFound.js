import React from 'react'
import style from '../Styles/NotFound.module.css'
import { Link } from '@reach/router'

// Página default

export default function NotFound() {
    return(
        <>
            <p className={style.notFound}>Tá perdido mano</p>
            <p><Link to="/main">Ir para a página dos Pokemons</Link></p>
            <p><Link to="/">Ir para a página de Login</Link></p>
        </>
    )
}