import React from 'react'
import api from '../services/api'
import { navigate } from '@reach/router'
import '../Styles/Login.css'

// Essa aqui é a página em que o usuário vai logar

export default function UserPage() {

    const [user, setUser] = React.useState({})

    React.useEffect(() => {
        let username = JSON.parse(localStorage.getItem('username'))
        if (username !== null) {
            navigate(`/main/${1}`)
        }
    }, [user])
    
    const handleSubmit = async (e) => { 
        e.preventDefault()
        console.log(user)
        
        localStorage.setItem('username', JSON.stringify(user.username))
        
        await api.get(`/users/${user.username}`)
        .then(response => navigate(`/main/${1}`))
        .catch(async error => {
            await api.post('/users', {user})
            navigate(`/main/${1}`)
        });
    }   

    return(
        <>
        <div className="login-container">
            <form onSubmit={handleSubmit} className="login">
                <div>
                    <h2>Olá! Digite seu nome de Treinador Pokémon:</h2>
                    <input type="text" onChange={(e) => setUser({username:e.target.value})}/>
                    <button type="submit">Enviar</button>
                </div>
            </form>
        </div>
        </>
    )
}