import React from 'react'
import api from '../services/api'
import PokeTag from '../Components/PokeTag'
import PokeProfile from '../Styles/PokeProfile.module.css'
import Load from '../images/loading.gif'
import { navigate } from '@reach/router'
let load = true;

// Essa aqui é a página em que o usuário vai ver o perfil

export default function UserPage({ username }) {
    const [starred, setStarred] = React.useState([])

    React.useEffect(() =>{
        const getStarred = async () => {
            const response = await api.get(`/users/${username}`)
            setStarred(response.data.pokemons)
        }
        getStarred();
        load = false;
    }, [username]);

    return (
        <>
            <nav className="navbar">
                <button className="navBtn" onClick={() => window.history.back()}>Voltar</button>
                <h1>Pokédex</h1>
                    <button className="navBtn" onClick={() => {localStorage.removeItem('username') 
            navigate('/')}}>Deslogar</button>
            </nav>

            { load ? <img src={Load} alt=""/> :
                <div className="main">
            
                    <h1>Olá, {username}! Essa é sua pagina de Treinador Pokémon</h1>
                    <h2>Esses são seus pokémons favoritos:</h2>
                    <div className={PokeProfile.subCont}>
                    {
                        starred.map((i) => <PokeTag main={1} poke={i} />)
                    }
                    </div>
            </div>
            }
        </>
    )
}