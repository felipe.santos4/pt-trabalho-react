import React from 'react'
import api from '../services/api'
import ButtonNav from '../Components/ButtonNav'
import '../Styles/Pokemon.css'
import { navigate } from '@reach/router'
import loading from '../images/loading.gif'
import PokeTag from '../Components/PokeTag'

//Essa aqui é a página principal

let load = true;

export default function MainPage() {

    let user = JSON.parse(localStorage.getItem('username')) || [];
    let userTeste = localStorage.getItem('username')
    const [pokemons, setPokemons] = React.useState([]);
    const [pages, setPages] = React.useState({ nextPage: '', prevPage: '' });
    console.log(user)
    console.log('User teste:',userTeste)

    const loadData = (page = 1) => {

        api.get(`/pokemons?page=${page}`)
        .then((response) => {
            setPokemons(response.data.data)
            console.log(response)
            load = false;
            setPages({ nextPage: response.data.next_page, prevPage: response.data.prev_page })
            navigate(`${page}`)
        })
    }

    React.useEffect(() => {
        loadData()
    }, [])

    const pokeNav = (page) => {
        if (page === null) return
        loadData(page)
    }

    return (
        <>
            <nav className="navbar">
                <h1>Pokédex</h1>
                <button className="navBtn" onClick={() => navigate(`/profile/${user}`)}>Perfil</button>
            </nav>
            
            <div className="main">

                { load ? <img src={ loading } alt="pikachu fofo" /> : 
                
                    <div className="container">

                        <ButtonNav label="Anterior" nav={pokeNav} pag={pages.prevPage} />

                        <div className="subCont">
                            { pokemons.map((poke) => {
                                return <PokeTag poke={poke} main={1} />
                            })}
                        </div>

                        <ButtonNav className={pages.pageNext ? "buttonHover" : ""} label="Próximo" nav={pokeNav} pag={pages.nextPage} />

                    </div>
                }
            </div>
        </>
    )
}