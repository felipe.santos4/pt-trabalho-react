import React from 'react'
import api from '../services/api'
import PokeInfoCard from '../Components/PokeInfoCard'
import '../Styles/Pokemon.css'

export default function PokeInfo({ name }) {
  const [isFav, setIsFav] = React.useState(false);
  const [pokeInfo, setPokeInfo] = React.useState({})
  const [arrPokeFav, setArrPokeFav] = React.useState([]);
  const usuario = JSON.parse(localStorage.getItem('username'));

  const procuraPokemon = () => {
    for (let i = 0; i < arrPokeFav.length; i++) {
      if (arrPokeFav[i].name === pokeInfo.name) {
        return true;
      }
    }
    return false;
  }

  const deleteFav = async () => {
    await api.delete(`/users/${usuario}/starred/${pokeInfo.name}`)
      .then((response) => {
        setIsFav(false);
        setArrPokeFav({ ...response.data.pokemons })
      }
      )
  }

  React.useEffect(() => {
    api.get(`/pokemons/${name}`)
      .then((response) => {
        setPokeInfo(response.data);
      })
      .catch(error => console.log(error))

    api.get(`/users/${usuario}`)
      .then((response) => {
        setArrPokeFav(response.data.pokemons)
      })
  }, [name, usuario])

  React.useEffect(() => {
    setIsFav(procuraPokemon);
  }, [arrPokeFav]);

  const addFavo = async () => {
    await api.post(`/users/${usuario}/starred/${pokeInfo.name}`)
      .then((response) => {
        setIsFav(true)
        setArrPokeFav(response.data.pokemons)
      })
      .catch(() => {
        deleteFav()
      })
  }

  return (
    <>
      <nav className="navbar">
        <button className="navBtn" onClick={() => window.history.back()}>Back to List</button>
        <h1>Pokédex</h1>
      </nav>
      <div className="main">

        <br />
        <br />

        <div className="subCont">
          <PokeInfoCard pokemon={pokeInfo} />
          <br />
        </div>

        <button className={isFav ? "redButtonFav" : "greenButtonFav"} onClick={() => addFavo()}>{isFav ? "Desfavoritar" : "Favoritar"}</button>
      </div>
    </>
  )
}