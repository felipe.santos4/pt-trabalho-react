import React from 'react'
import '../Styles/PokeInfoCard.css'
import mainTag from '../Components/MainTag'
import valida from '../Components/VerificaPokeKind'
import loading from '../images/loading.gif'

// Componente utilizado pra renderizar a página do perfil do pokemon

const PokeInfoCard = (pokemon) => {

  if(!valida(pokemon.pokemon.kind)) {
    return(
      <img src={loading} alt="#" />
    )
  }

  var tipos = pokemon.pokemon.kind.split(";");

  return (
    <>
    <div className="pokeInfo">
      
        <div className="image-container">
          <img src={pokemon.pokemon.image_url} alt=""/>
        </div>
        <div className="info-container">
          <h3>{pokemon.pokemon.name}</h3>
          <div className="stats">
            <p>Peso: {pokemon.pokemon.weight}</p>
            <p>Altura: {pokemon.pokemon.height}</p>
            { tipos.map((item) => mainTag(item)) }
          </div>
        </div>
      
    </div>
    </>
  )
}

export default PokeInfoCard