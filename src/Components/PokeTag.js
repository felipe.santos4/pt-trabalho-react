import React from 'react'
import mainTag from '../Components/MainTag'
import '../Styles/Pokemon.css'
import '../Styles/Pokemon.css'
import Verifica from '../Components/VerificaPokeKind'
import { navigate } from '@reach/router'
import loading from '../images/loading.gif'
import swColor from '../Components/SwitchColors'
import {PokeCard} from '../Styles/PokeCard'

export default function pokeTag ({poke, main}) {

    if (!Verifica(poke.kind)){ 
        return(
            <img alt="#" src={loading}/>
        )
    }

    var tipos = poke.kind.split(";");

    return(
        <PokeCard changeGradient={swColor(tipos[0])} changeGradient2 = { tipos[1] ? swColor(tipos[1]) : null } key={poke.id} onClick={ main ?  () => navigate(`/pokeinfo/${poke.name}`) : null}>
            <img src={poke.image_url} alt="" />
            <p> {poke.name}</p>
            { tipos.map((item, i) => mainTag(item)) }
        </PokeCard>
    )
}