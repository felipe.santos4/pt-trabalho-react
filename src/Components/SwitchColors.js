export default function swColor(tipo) {
    switch (tipo) {
        case 'grass':
            return '#9bcc50';
        case 'fire':
            return '#fd7d24';
        case 'poison':
            return '#b97fc9';
        case 'flying':
            return '#418294';
        case 'water':
            return '#4592c4';
        case 'bug':
            return '#589413';
        case 'normal':
            return '#a4acaf';
        case 'electric':
            return '#eed535';
        case 'ground':
            return '#ab9842';
        case 'fairy':
            return '#fdb9e9';
        case 'fighting':
            return '#ff6200';
        case 'psychic':
            return '#f366b9';
        case 'rock':
            return '#746217';
        case 'steel':
            return '#9eb7b8';
        case 'ice':
            return '#51c4e7';
        case 'ghost':
            return '#7b62a3';
        case 'dragon':
            return '#f16e57';
        case 'dark':
            return '#707070';
        default:
            return;
    }
}