import React from 'react'
import '../Styles/Pokemon.css'

export default function ButtonNav({ label, nav, pag, click }) {

    return <button className={pag ? "buttonHover" : ""} disabled={click} onClick={() => nav(pag)}>{label}</button>
}