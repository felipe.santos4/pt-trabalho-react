import React from 'react'
import Tag from '../Styles/Tag'
import swColor from '../Components/SwitchColors'

// Função para retornar os tipos dos pokemons coloridos

export default function MainTag(item) {
    return (
        <>
        <span
        style={Tag(swColor(item))}> {item}
        </span>
        <span> </span>
        </>
    )
}